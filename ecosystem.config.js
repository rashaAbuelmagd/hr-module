const env = 'development';

const devOptions = {
  watch: ['controllers', 'middlewares', 'models', 'routes', 'services', 'test'],
  ignore_watch: ['node_modules']
};

const prodOptions = {
  instances: "max",
  autorestart: true
};

module.exports = {
  apps: [
    {
      name: "HR-Module",
      script: "bin/www",
      ...(env === 'production' ? prodOptions : devOptions)
    }
  ]
}