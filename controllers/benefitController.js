const BenefitService = require('../services/benefitService')


/**
   * @api {get} /benefits Get All Benefits
   * @apiGroup Benefit
   * @apiVersion 1.0.0
   * @apiHeader {String} Authorization unique access-key.
   * @apiHeaderExample {json} Header-Example:
   *     {
   *       "Authorization":"kkNRuRurO0lX7N24"
   *     }
   * @apiSuccessExample {json} Success-Response:
   *   HTTP/1.1 200 OK
   * {
   *  "message": "All Benefits",
   *  "data": [{
   *       "id": 1,
   *       "name": "medright",
   *       "type": "medical",
   *       "description": "Bla bla bla",
   *       "updatedAt": "2019-10-14T12:35:07.189Z",
   *       "createdAt": "2019-10-14T12:35:07.189Z"
   *    }]
   * }
   * @apiError BenefitNotFound
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 404 Not Found
   *     {
   *       "message": "Empty Benefits",
   *       "data":[]
   *     }
   * @apiError AuthError
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 400 Bad Request
   *     "success": false,
   *     "error":{
   *       "message": "Token not found in request headers",
   *     }
   * 
   * @apiError AuthorizationError
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 403 Forbidden
   *     "success": false,
   *     "error":{
   *       "message": "Forbidden, You don't have permission to access",
   *     }
   */
exports.index = async (req, res) => {

    const benefits = await BenefitService.read()

    if (!benefits) {
        return res.status(404).json({
            message: 'Empty Benefits',
            data: []
        });
    }

    res.status(200).json({
        message: 'All Benefits',
        data: benefits
    });
}

/**
   * @api {post} /benefits Add New Benefit
   * @apiGroup Benefit
   * @apiVersion 1.0.0
   * @apiHeader {String} Authorization unique access-key.
   * @apiHeaderExample {json} Header-Example:
   *     {
   *       "Authorization":"kkNRuRurO0lX7N24"
   *     }
   * @apiParam {String} body Post Body.
   * @apiParamExample {json} Request-Example:
   *    {
   *       "name": "medright",
   *       "type": "medical",
   *       "description": "Bla bla bla",
   *    }
   * 
   * @apiSuccessExample {json} Success-Response:
   *   HTTP/1.1 200 OK
   * 
   * {
   *    "message": "New Benefit Added Successfully",
   *    "data": {
   *       "id": 1,
   *       "name": "medright",
   *       "type": "medical",
   *       "description": "Bla bla bla",
   *       "updatedAt": "2019-10-14T12:35:07.189Z",
   *       "createdAt": "2019-10-14T12:35:07.189Z"
   *     }
   * }
   * @apiError AddBenefitError
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 500 
   *     {
   *       "message": "Error in saving data",
   *       "data":"Error"
   *     }
   * @apiError AuthError
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 400 Bad Request
   *     "success": false,
   *     "error":{
   *       "message": "Token not found in request headers",
   *     }
   * 
   * @apiError AuthorizationError
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 403 Forbidden
   *     "success": false,
   *     "error":{
   *       "message": "Forbidden, You don't have permission to access",
   *     }
   */
exports.create = async (req, res) => {

    const benefitData = req.body;

    try {
        const benefit = await BenefitService.store(benefitData)

        res.status(200).json({
            message: 'New Benefit Added Successfully',
            data: benefit
        });

    } catch (error) {
        res.status(500).json({
            message: 'Error in saving data',
            data: error
        });
    }

};

/**
   * @api {get} /benefits/medical Get Benefit By Type
   * @apiGroup Benefit
   * @apiVersion 1.0.0
   * @apiHeader {String} Authorization unique access-key.
   * @apiHeaderExample {json} Header-Example:
   *     {
   *       "Authorization":"kkNRuRurO0lX7N24"
   *     }
   * @apiSuccessExample {json} Success-Response:
   *   HTTP/1.1 200 OK
   * {
   *  "message": "Benefits returned successfully",
   *  "data": [{
   *       "id": 1,
   *       "name": "medright",
   *       "type": "medical",
   *       "description": "Bla bla bla",
   *       "updatedAt": "2019-10-14T12:35:07.189Z",
   *       "createdAt": "2019-10-14T12:35:07.189Z"
   *     }]
   * }
   * @apiError BenefitNotFound
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 404 Not Found
   *     {
   *       "message": "Benefit Not Found",
   *       "data":[]
   *     }
   * @apiError AuthError
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 400 Bad Request
   *     "success": false,
   *     "error":{
   *       "message": "Token not found in request headers",
   *     }
   */
exports.get = async (req, res) => {

    const benefitType = req.params.type

    const benefits = await BenefitService.find({ 'type': benefitType })

    if (!benefits) {
        return res.status(404).json({
            message: 'Benefit Not Found',
            data: []
        });
    }

    res.status(200).json({
        message: 'Benefits returned successfully',
        data: benefits
    });
}

/**
   * @api {put} /benefits/1 Update benefit
   * @apiGroup Benefit
   * @apiVersion 1.0.0
   * @apiHeader {String} Authorization unique access-key.
   * @apiHeaderExample {json} Header-Example:
   *     {
   *       "Authorization":"kkNRuRurO0lX7N24"
   *     }
   * @apiParam {String} body Post Body.
   * @apiParamExample {json} Request-Example:
   *    {
   *       "name": "medright 2",
   *       "type": "medical",
   *       "description": "Bla bla bla bla",
   *    }
   * @apiSuccessExample {json} Success-Response:
   *   HTTP/1.1 200 OK
   * {
   *  "message": "Benefit updated successfully",
   *  "data": [1]
   * }
   * @apiError AuthError
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 400 Bad Request
   *     "success": false,
   *     "error":{
   *       "message": "Token not found in request headers",
   *     }
   * @apiError AuthorizationError
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 403 Forbidden
   *     "success": false,
   *     "error":{
   *       "message": "Forbidden, You don't have permission to access",
   *     }
   */
exports.update = async (req, res) => {

    const benefitData = req.body;
    const benefitId = req.params.benefitId

    const benefit = await BenefitService.update(benefitData, { 'id': benefitId })

    res.status(200).json({
        message: 'Benefit updated successfully',
        data: benefit
    });
}

/**
   * @api {delete} /benefits/1 Delete Benefit
   * @apiGroup Benefit
   * @apiVersion 1.0.0
   * @apiHeader {String} Authorization unique access-key.
   * @apiHeaderExample {json} Header-Example:
   *     {
   *       "Authorization":"kkNRuRurO0lX7N24"
   *     }
   * @apiSuccessExample {json} Success-Response:
   *   HTTP/1.1 200 OK
   * {
   *  "message": "Benefit deleted successfully",
   *  "data": 1
   * }
   * @apiError AuthError
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 400 Bad Request
   *     "success": false,
   *     "error":{
   *       "message": "Token not found in request headers",
   *     }
   * @apiError AuthorizationError
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 403 Forbidden
   *     "success": false,
   *     "error":{
   *       "message": "Forbidden, You don't have permission to access",
   *     }
   */
exports.destroy = async (req, res) => {

    const benefitId = req.params.benefitId

    const benefit = await BenefitService.destroy({ 'id': benefitId })

    res.status(200).json({
        message: 'Benefit deleted successfully',
        data: benefit
    });
}