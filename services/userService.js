const models = require('../models/')

exports.find = async (user) => {

    return await models.User.findOne({ where: user })

}

exports.store = async (userData) => {

    return await models.User.create(userData)

}

exports.read = async () => {

    return await models.User.findAll()

}

exports.update = async (attributes, user) => {

    return await models.User.update(attributes, {where: user})

}

exports.destroy = async (user) => {

    return await models.User.destroy({ where: user })

}