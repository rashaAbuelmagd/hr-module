const models = require('../models/')

exports.find = async (benefit) => {

    return await models.Benefit.findAll({ where: benefit })

}

exports.store = async (benefit) => {

    return await models.Benefit.create(benefit)

}

exports.read = async () => {

    return await models.Benefit.findAll()

}

exports.update = async (attributes, benefit) => {

    return await models.Benefit.update(attributes, {where: benefit})

}

exports.destroy = async (benefit) => {

    return await models.Benefit.destroy({ where: benefit })

}