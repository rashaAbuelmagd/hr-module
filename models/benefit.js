'use strict';
module.exports = (sequelize, DataTypes) => {
  const Benefit = sequelize.define('Benefit', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    type: {
      type: DataTypes.ENUM,
      values: ['offer', 'medical', 'news', 'other']
    }
  }, {});
  Benefit.associate = function (models) {
    Benefit.hasMany(models.User, {
      foreignKey: 'dapartmentid',
      as: 'benefits'
    });
  };
  return Benefit;
};