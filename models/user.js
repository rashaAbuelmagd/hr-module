'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    managerid: DataTypes.INTEGER,
    dapartmentid: DataTypes.INTEGER,
    isadmin: DataTypes.BOOLEAN,
  }, {});
  User.associate = function (models) {
    User.belongsTo(models.Benefit, {
      foreignKey: 'dapartmentid'
    });
  };
  return User;
};