var express = require('express');
var router = express.Router();

const userController = require('../controllers/userController');
const middleware = require('../middlewares/Authenticate');
const authorization = require("../middlewares/Authorization");


router.post('/login', userController.login);
router.put('/:userId', middleware.Authenticate, userController.update);

router.post('/', middleware.Authenticate, authorization.Authorize, userController.create);
router.delete('/:userId', middleware.Authenticate, authorization.Authorize, userController.destroy);
router.get('/', middleware.Authenticate, authorization.Authorize, userController.index);
router.get('/:username', middleware.Authenticate, authorization.Authorize, userController.get);

module.exports = router;
