var express = require('express');
var router = express.Router();

const benefitController = require('../controllers/benefitController');
const middleware = require('../middlewares/Authenticate');
const authorization = require("../middlewares/Authorization");

router.get('/:type', middleware.Authenticate, benefitController.get);

router.post('/', middleware.Authenticate, authorization.Authorize, benefitController.create);

router.put('/:benefitId', middleware.Authenticate, authorization.Authorize, benefitController.update);
router.delete('/:benefitId', middleware.Authenticate, authorization.Authorize, benefitController.destroy);

router.get('/', middleware.Authenticate, authorization.Authorize, benefitController.index);

module.exports = router;
