'use strict';
process.env.NODE_ENV = 'test';
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');
const expect = chai.expect;
let accessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjcsInVzZXJuYW1lIjoidGVzdFRlc3QiLCJpYXQiOjE1NjY2NzQwMjV9.PeYW_75oUPOKzTxiEI4CGJJDdGG5Ppldu7iQnRXgygE';
chai.use(chaiHttp);

describe('User Controller Test', function () {
    it('Should Save Data Into Db ', function () {
        return chai.request(app)
            .post('/users/')
            .send({
                "firstname": "Test",
                "lastname": "Test1",
                "username": "testTest",
                "email": "test@test.com",
                "password": "123456",
                "dapartmentid": "5",
                "isadmin": true
            })
            .then(function (res) {
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('message').eql('Account Created Successfully');
                expect(res.body).to.have.property('data');
                expect(res).to.be.json;
            }).catch(function (err) {
                expect(err).to.have.status(200);
                expect(res.body).to.have.property('message');
            });
    });

    it('Should Update User Data', function () {
        return chai.request(app)
            .put('/users/1')
            .set('Authorization', accessToken)
            .send({
                "firstname": "Test",
                "lastname": "Test1",
                "username": "testTest",
                "email": "test@test.com",
                "password": "123456",
                "dapartmentid": "5",
                "isadmin": true
            })
            .then(function (res) {
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('message').eql('User updated successfully');
                expect(res.body).to.have.property('data');
            }).catch(function (err) {
                expect(err).to.have.status(400);
                expect(res.body).to.have.property('message').eql('Token not found in request headers');
            });
    });
})